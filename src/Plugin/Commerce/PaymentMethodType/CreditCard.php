<?php

namespace Drupal\commerce_globalpayments\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;
use Drupal\entity\BundleFieldDefinition;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;

/**
 * Provides the Global Payments payment method type for credit cards.
 *
 * @CommercePaymentMethodType(
 *   id = "globalpayments_credit_card",
 *   label = @Translation("Global Payments"),
 * )
 */
class CreditCard extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    $args = [
      '@card_type' => $payment_method->card_type->value,
      '@card_number' => $payment_method->card_number->value,
    ];
    return $this->t('**** @card_number (@card_type)', $args);
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    // Store card type and last 4 digits of the card for logging purposes.
    $fields['card_type'] = BundleFieldDefinition::create('list_string')
      ->setLabel(t('Card type'))
      ->setDescription(t('The credit card type.'))
      ->setSetting('allowed_values_function', ['\Drupal\commerce_payment\CreditCard', 'getTypeLabels']);
    $fields['card_number'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Card number'))
      ->setDescription(t('The last four digits of the credit card number.'));
    $fields['card_exp_month'] = BundleFieldDefinition::create('integer')
      ->setLabel(t('Card expiration month'))
      ->setDescription(t('The credit card expiration month.'))
      ->setSetting('size', 'tiny');
    $fields['card_exp_year'] = BundleFieldDefinition::create('integer')
      ->setLabel(t('Card expiration year'))
      ->setDescription(t('The credit card expiration year.'))
      ->setSetting('size', 'small');

    // For delayed transactions we need to store card and payer references.
    // See https://developer.realexpayments.com/#!/integration-api/card-storage/php/html_js
    $fields['card_reference'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Card Reference'))
      ->setDescription(t('Reference to the card in Global Payments.'));
    $fields['payer_reference'] = BundleFieldDefinition::create('string')
      ->setLabel(t('Payer Reference'))
      ->setDescription(t('Reference to the payer in Global Payments.'));

    // HPP payment flow requires some JSON to be generated by Global Payments
    // and then used on the fronted for rendering secure payment form.
    $fields['remote_json'] = BundleFieldDefinition::create('text_long')
      ->setLabel(t('Remote JSON'))
      ->setDescription(t('Remote JSON payload from Global Payments.'));

    return $fields;
  }

}
