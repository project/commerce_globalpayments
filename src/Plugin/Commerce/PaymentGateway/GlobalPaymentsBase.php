<?php

namespace Drupal\commerce_globalpayments\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\HardDeclineException;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayBase;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides base class for Global Payments (Realex) payments.
 */
abstract class GlobalPaymentsBase extends OnsitePaymentGatewayBase {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    // Do not initialize payment gateway if it's not configured yet.
    if (empty($configuration['mode']) || empty($configuration['secret'])) {
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'merchant_id' => '',
      'account_id' => '',
      'secret' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant ID'),
      '#default_value' => $this->configuration['merchant_id'],
      '#required' => TRUE,
    ];

    $form['account_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account ID'),
      '#default_value' => $this->configuration['account_id'],
      '#required' => TRUE,
    ];

    $form['secret'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shared secret'),
      '#default_value' => $this->configuration['secret'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['merchant_id'] = $values['merchant_id'];
      $this->configuration['account_id'] = $values['account_id'];
      $this->configuration['secret'] = $values['secret'];
    }
  }

  /**
   * Format amount for Global Payments API (multiplied by 100).
   *
   * @param float $amount
   *   Amount.
   *
   * @return float|int
   *   Formatted amount.
   */
  public static function formatAmount($amount) {
    return $amount * 100;
  }

  /**
   * Format expiration data for Global Payments API (MMYY).
   *
   * @param string $month
   *   Month number in MM format.
   * @param string $year
   *   Year number in YY format.
   *
   * @return string
   *   Formatted expiry for Realex API.
   */
  public static function formatExpiry($month, $year) {
    return $month . substr($year, -2);
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
    // TODO.
    throw new HardDeclineException('The method is not implemented yet.');
  }

  /**
   * {@inheritdoc}
   */
  public function deletePaymentMethod(PaymentMethodInterface $payment_method) {
    $payment_method->delete();
  }

  /**
   * Returns payment method id which can be used as a reference in Realex.
   *
   * @param string $suffix
   *   Any suffix to be added to the reference.
   *
   * @return string
   *   Payment method UUID prefixed with payer type from gateway settings.
   */
  public function getReference(string $suffix) {
    if (empty($this->configuration['payer_type'])) {
      return FALSE;
    }
    return date('ymd') . '_' . strtoupper($this->configuration['payer_type']) . '_' . strtoupper($suffix);
  }

}
