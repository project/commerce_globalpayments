<?php

namespace Drupal\commerce_globalpayments\Normalizer;

use Drupal\serialization\Normalizer\ComplexDataNormalizer;

/**
 * Attach extra data to payment entity returned via Decoupled Commerce Checkout.
 */
class PaymentEntityNormalizer extends ComplexDataNormalizer {

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = 'Drupal\commerce_payment\Entity\PaymentInterface';

  /**
   * The format that the Normalizer can handle.
   *
   * @var array
   */
  protected $format = ['json'];

  /**
   * {@inheritdoc}
   */
  public function normalize($object, $format = NULL, array $context = []): \ArrayObject | array | string | int | float | bool | NULL {
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $entity */
    $payment_method = $entity->getPaymentMethod();
    $plugin_id = $entity->getPaymentGateway()->getPluginId();
    $payment_id = $entity->id();

    if (strpos($plugin_id, 'globalpayments_hpp_creditcard') !== 0) {
      return parent::normalize($entity, $format, $context);
    }
    if (!$payment_method->hasField('remote_json')) {
      return parent::normalize($entity, $format, $context);
    }

    $remote_json = \GuzzleHttp\json_decode($payment_method->get('remote_json')->value, TRUE);

    return [
      'payment_id' => [0 => ['value' => $payment_id]],
      'remote_json' => $remote_json,
    ];
  }

}
