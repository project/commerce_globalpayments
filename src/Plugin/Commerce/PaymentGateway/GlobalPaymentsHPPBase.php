<?php

namespace Drupal\commerce_globalpayments\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\Core\Form\FormStateInterface;
use GlobalPayments\Api\ServiceConfigs\Gateways\GpEcomConfig;
use GlobalPayments\Api\Entities\Address;
use GlobalPayments\Api\Entities\HostedPaymentData;
use GlobalPayments\Api\ServicesContainer;

/**
 * Provides base class for Global Payments (Realex) HPP payments.
 */
abstract class GlobalPaymentsHPPBase extends GlobalPaymentsBase {

  const LIVE_HPP_ENDPOINT = 'https://pay.realexpayments.com/pay';
  const TEST_HPP_ENDPOINT = 'https://pay.sandbox.realexpayments.com/pay';

  const LIVE_API_ENDPOINT = 'https://api.realexpayments.com/epage-remote.cgi';
  const TEST_API_ENDPOINT = 'https://api.sandbox.realexpayments.com/epage-remote.cgi';

  const MODE_HPP = 'HPP';
  const MODE_API = 'API';

  /**
   * Returns GP service config initialized with gateway configurations.
   *
   * Can be in HPP mode (for working with Hosted Payment Payments) or in API
   * mode for working with other APIs.
   *
   * @param string $mode
   *   HPP or API mode.
   *
   * @return \GlobalPayments\Api\ServiceConfigs\Gateways\GpEcomConfig
   *   Global Payments Realex config.
   */
  protected function initServiceInstance($mode = self::MODE_HPP) {
    $config = new GpEcomConfig();
    $config->merchantId = $this->configuration['merchant_id'];
    $config->accountId = $this->configuration['account_id'];
    $config->sharedSecret = $this->configuration['secret'];
    if ($mode == self::MODE_HPP) {
      $config->serviceUrl = $this->configuration['mode'] === 'live' ? self::LIVE_HPP_ENDPOINT : self::TEST_HPP_ENDPOINT;
    }
    else {
      $config->serviceUrl = $this->configuration['mode'] === 'live' ? self::LIVE_API_ENDPOINT : self::TEST_API_ENDPOINT;
    }
    ServicesContainer::configureService($config);

    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'button_label' => 'Pay',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['button_label'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Pay button label'),
      '#default_value' => $this->configuration['button_label'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['button_label'] = $values['button_label'];
    }
  }

  /**
   * Set Global Payments HPP fields related to personal user data.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentInterface $payment
   *   Payment.
   * @param \GlobalPayments\Api\Entities\HostedPaymentData $hpp_payment_data
   *   Global Payments payment data object (will be modified).
   *
   * @return \GlobalPayments\Api\Entities\Address
   *   Returns Global Payments address data object.
   */
  protected function prepareBillingData(PaymentInterface $payment, HostedPaymentData $hpp_payment_data) {
    $payment_method = $payment->getPaymentMethod();
    $order = $payment->getOrder();

    $hpp_payment_data->customerEmail = $order->getEmail();
    $hpp_billing_address = new Address();

    // Let other modules prepare HPP data in required format.
    \Drupal::moduleHandler()->alter('hpp_payment_data', $hpp_payment_data, $hpp_billing_address, $payment_method);

    // Sanitizing address fields as we Global Payments requirements.
    // See details here: https://developer.globalpay.com/hpp/3d-secure-two
    if ($hpp_billing_address->streetAddress1) {
      $hpp_billing_address->streetAddress1 = self::sanitizeStringForHpp($hpp_billing_address->streetAddress1);
    }
    if ($hpp_billing_address->streetAddress2) {
      $hpp_billing_address->streetAddress2 = self::sanitizeStringForHpp($hpp_billing_address->streetAddress2);
    }
    if ($hpp_billing_address->streetAddress3) {
      $hpp_billing_address->streetAddress3 = self::sanitizeStringForHpp($hpp_billing_address->streetAddress3);
    }
    if ($hpp_billing_address->city) {
      $hpp_billing_address->city = self::sanitizeStringForHpp($hpp_billing_address->city, 'normal', 40);
    }
    if ($hpp_billing_address->postalCode) {
      $hpp_billing_address->postalCode = self::sanitizeStringForHpp($hpp_billing_address->postalCode, 'strict', 16);
    }
    if ($hpp_payment_data->customerPhoneMobile) {
      $hpp_payment_data->customerPhoneMobile = substr($hpp_payment_data->customerPhoneMobile, 0, 19);
    }

    // Some fields can become empty after sanitizing and it will not allow HPP
    // to load. To avoid it, pass fake data in required fields.
    // It should be safe enough, because the user already managed to provide
    // bad data that didn't pass validation, e.g "!!!!!" instead of city.
    $using_fake = FALSE;
    if (!$hpp_billing_address->streetAddress1) {
      $hpp_billing_address->streetAddress1 = "Flat 123";
      $using_fake = TRUE;
    }
    if (!$hpp_billing_address->city) {
      $hpp_billing_address->city = "Halifax";
      $using_fake = TRUE;
    }
    if (!$hpp_billing_address->postalCode) {
      $hpp_billing_address->postalCode = "W5 9HR";
      $using_fake = TRUE;
    }
    if (!$hpp_payment_data->customerPhoneMobile) {
      $hpp_payment_data->customerPhoneMobile = '44|07123456789';
      $using_fake = TRUE;
    }

    if ($using_fake) {
      \Drupal::logger('commerce_globalpayments')->error('Some Global Payments HPP fields use fake data because provided data was invalid. Order: @id', ['@id' => $payment->getOrderId()]);
    }

    return $hpp_billing_address;
  }

  /**
   * Sanitizes user input to comply with Global Payments HPP requirements.
   *
   * @param string $string
   *   String to sanitize.
   * @param string $mode
   *   Either 'normal' (for most of the fields) or 'strict' (for postal code).
   * @param int $max_length
   *   Max allowed length of the string.
   *
   * @return false|string
   *   Sanitized string.
   */
  public static function sanitizeStringForHpp($string, $mode = 'normal', $max_length = 50) {
    $regexp = $mode === 'strict' ? '/[^\w\d\-\s]/' : "/[^\w\d\/\-\_\.\s\(\)\':;]/u";
    return mb_substr(preg_replace($regexp, '', $string), 0, $max_length);
  }

}
