<?php

namespace Drupal\commerce_globalpayments\Plugin\Commerce\PaymentGateway;

use com\realexpayments\remote\sdk\domain\payment\PaymentRequest;
use com\realexpayments\remote\sdk\http\HttpConfiguration;
use com\realexpayments\remote\sdk\RealexClient;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides base class for Global Payments (Realex) payments.
 */
abstract class GlobalPaymentsApiBase extends GlobalPaymentsBase {

  const LIVE_ENDPOINT = 'https://api.realexpayments.com/epage-remote.cgi';
  const TEST_ENDPOINT = 'https://api.sandbox.realexpayments.com/epage-remote.cgi';

  /**
   * Realex SDK API client.
   *
   * @var \com\realexpayments\remote\sdk\RealexClient
   */
  protected $api;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);

    $this->initApi($configuration['mode'], $configuration['secret']);
  }

  /**
   * Prepare Realex SDK.
   *
   * @param string $mode
   *   Payment gateway mode (live or test).
   * @param string $secret
   *   Realex API secret.
   */
  protected function initApi($mode, $secret) {
    if (empty($secret)) {
      return;
    }

    $httpConfig = new HttpConfiguration();
    $endpoint = $mode === 'live' ? self::LIVE_ENDPOINT : self::TEST_ENDPOINT;
    $httpConfig->setEndpoint($endpoint);
    $this->api = new RealexClient($secret, $httpConfig);
  }

  /**
   * Makes authenticated Realex API requests using SDK.
   */
  protected function request() {
    return (new PaymentRequest())
      ->addMerchantId($this->configuration['merchant_id'])
      ->addAccount($this->configuration['account_id']);
  }

}
