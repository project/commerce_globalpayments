<?php

namespace Drupal\commerce_globalpayments\Plugin\Commerce\PaymentGateway;

use com\realexpayments\remote\sdk\domain\Card;
use com\realexpayments\remote\sdk\domain\CardType;
use com\realexpayments\remote\sdk\domain\payment\AutoSettle;
use com\realexpayments\remote\sdk\domain\payment\AutoSettleFlag;
use com\realexpayments\remote\sdk\domain\payment\PaymentType;
use com\realexpayments\remote\sdk\domain\PresenceIndicator;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsAuthorizationsInterface;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides Global Payments (Realex) payment gateway for credit cards.
 *
 * @CommercePaymentGateway(
 *   id = "globalpayments_creditcard",
 *   label = "Global Payments Credit Card",
 *   display_label = "Global Payments Credit Card",
 *   payment_method_types = {"globalpayments_credit_card"},
 * )
 */
class GlobalPaymentsCreditCard extends GlobalPaymentsApiBase implements SupportsAuthorizationsInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'card_types' => [],
    ] + parent::defaultConfiguration();
  }

  /**
   * Builds credit card select list for payment gateway configuration form.
   */
  public function cardTypesFormOptions() {
    $options = [];
    foreach (CreditCard::getTypes() as $cardType) {
      $options[$cardType->getId()] = $cardType->getLabel();
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['card_types'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Card types'),
      '#description' => $this->t('You can leave this field empty if you want to accept all card types.'),
      '#default_value' => $this->configuration['card_types'],
      '#options' => $this->cardTypesFormOptions(),
      '#size' => count($this->cardTypesFormOptions()),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['card_types'] = $values['card_types'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    $required_keys = [
      'name', 'number', 'expiration', 'security_code',
    ];
    foreach ($required_keys as $required_key) {
      if (empty($payment_details[$required_key])) {
        throw new \InvalidArgumentException(sprintf('$payment_details must contain the %s key.', $required_key));
      }
    }

    // Validate the card using Drupal Commerce utilities.
    $card_type = CreditCard::detectType($payment_details['number']);
    if (!in_array($card_type->getId(), $this->getCreditCardTypes())) {
      throw new DeclineException(sprintf('Unsupported card type.'));
    }
    if (!CreditCard::validateNumber($payment_details['number'], $card_type)) {
      throw new DeclineException('Invalid card number.');
    }
    if (!CreditCard::validateExpirationDate($payment_details['expiration']['month'], $payment_details['expiration']['year'])) {
      throw new DeclineException('The card has expired.');
    }
    if (!CreditCard::validateSecurityCode($payment_details['security_code'], $card_type)) {
      throw new DeclineException('Invalid security code.');
    }

    // Save card type and the last 4 numbers of card number.
    $payment_method->setReusable(FALSE);
    $payment_method->card_type = $card_type->getId();
    $payment_method->card_number = substr($payment_details['number'], -4);
    $payment_method->card_exp_month = $payment_details['expiration']['month'];
    $payment_method->card_exp_year = $payment_details['expiration']['year'];
    $payment_method->save();

    // Pass payment details further. It won't save anything in the database
    // because payment method type doesn't have fields apart from these:
    // PaymentMethodType\CreditCard::buildFieldDefinitions.
    $payment_method->payment_details = $payment_details;
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);
    $payment_details = $payment_method->payment_details;

    // Save payment in new state for better logging.
    $payment->setState('new');
    $payment->save();

    // Format amount and expiry for Global Payments API.
    $amount = self::formatAmount($payment->getAmount()->getNumber());
    $expiry = self::formatExpiry($payment_details['expiration']['month'], $payment_details['expiration']['year']);

    // Based on $capture flag we either settle the transaction immediately or
    // open it for a settle in capturePayment() method.
    $settle = $capture ? AutoSettleFlag::TRUE : AutoSettleFlag::FALSE;

    $card = (new Card())
      ->addNumber($payment_details['number'])
      ->addType(self::formatCardType($payment_method->card_type->value))
      ->addCardHolderName($payment_details['name'])
      ->addCvn($payment_details['security_code'])
      ->addCvnPresenceIndicator(PresenceIndicator::CVN_PRESENT)
      ->addExpiryDate($expiry);

    $request = $this->request()
      ->addType(PaymentType::AUTH)
      ->addCard($card)
      ->addAmount($amount)
      ->addCurrency($payment->getAmount()->getCurrencyCode())
      ->addAutoSettle((new AutoSettle())->addFlag($settle));

    $response = $this->api->send($request);
    if (!$response->isSuccess()) {
      \Drupal::logger('commerce_globalpayments')->error(
        'Credit card payment error from Global Payments API: @message (code @status_code / order @order_id)',
        [
          '%id' => $payment->id(),
          '@status_code' => $response->getResult(),
          '@message' => $response->getMessage(),
          '@order_id' => $payment->getOrderId(),
          'link' => $payment->getOrder()->toLink('View order')->toString(),
        ]
      );
      throw new DeclineException('Could not process credit card payment.');
    }

    $expires = CreditCard::calculateExpirationTimestamp($payment_details['expiration']['month'], $payment_details['expiration']['year']);
    $payment_method->setExpiresTime($expires);
    // Global Payments payments id is stored on payment method level.
    $payment_method->setRemoteId($response->getPaymentsReference());
    $payment_method->save();

    $next_state = $capture ? 'completed' : 'authorization';
    $payment->setState($next_state);
    // Global Payments order id is stored on payment level.
    $payment->setRemoteId($response->getOrderId());
    $payment->save();

  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $this->assertPaymentState($payment, ['authorization']);
    $payment_method = $payment->getPaymentMethod();
    // If not specified, capture the entire amount.
    $amount = $amount ?: $payment->getAmount();

    $request = $this->request()
      ->addType(PaymentType::SETTLE)
      ->addAmount(self::formatAmount($amount->getNumber()))
      ->addOrderId($payment->getRemoteId())
      ->addPaymentsReference($payment_method->getRemoteId());

    $response = $this->api->send($request);

    if (!$response->isSuccess()) {
      throw new DeclineException('Could not complete credit card payment.');
    }

    $payment->setState('completed');
    $payment->setAmount($amount);
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getCreditCardTypes() {
    if (!empty($this->configuration['card_types'])) {
      return $this->configuration['card_types'];
    }

    // Enable all card types by default.
    return CreditCard::getTypes();
  }

  /**
   * Format credit card type for Global Payments API.
   *
   * @param string $commerce_type
   *   Credit card type as defined in Drupal Commerce.
   *
   * @return string|bool
   *   Realex credit card type, or FALSE if mapping not found.
   */
  public static function formatCardType($commerce_type) {
    $mapping = [
      'amex' => CardType::AMEX,
      'dinersclub' => CardType::DINERS,
      'jcb' => CardType::JCB,
      'maestro' => CardType::MASTERCARD,
      'mastercard' => CardType::MASTERCARD,
      'visa' => CardType::VISA,
      'discover' => 'DISCOVER',
    ];

    return !empty($mapping[$commerce_type]) ? $mapping[$commerce_type] : FALSE;
  }

}
