<?php

namespace Drupal\commerce_globalpayments\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_price\Price;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\CreditCard;
use GlobalPayments\Api\Entities\Customer;
use GlobalPayments\Api\Entities\Enums\AddressType;
use GlobalPayments\Api\Entities\Enums\HppVersion;
use GlobalPayments\Api\Entities\HostedPaymentData;
use GlobalPayments\Api\HostedPaymentConfig;
use GlobalPayments\Api\Services\HostedService;

/**
 * Provides Global Payments HPP credit card storage gateway.
 *
 * IMPORTANT: it does NOT charge the card. Instead, it validates the card and
 * saves it in Realex Vault for later processing. You can use this gateway if
 * you need to collect cards data and then process payments either in Realex
 * Control Panel or using your custom integration.
 * See https://developer.globalpay.com/#!/hpp/card-storage#hpp-create-payer.
 *
 * @CommercePaymentGateway(
 *   id = "globalpayments_hpp_creditcard_storage",
 *   label = "Global Payments HPP - Store Credit Card in Realex vault",
 *   display_label = "Global Payments HPP - Store Credit Card in Realex vault",
 *   payment_method_types = {"globalpayments_credit_card"},
 * )
 */
class GlobalPaymentsHPPCreditCardStorage extends GlobalPaymentsHPPCreditCard {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'payer_type' => 'Drupal',
      'otb_check' => 0,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['payer_type'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Payer type in Global Payments'),
      '#description' => $this->t('Created payers will be marked with the specified type in <a href="@link">RealControl</a>. It may be useful for managing payers from different sources.', ['@link' => 'https://realcontrol.realexpayments.com']),
      '#default_value' => $this->configuration['payer_type'],
    ];

    $form['otb_check'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Open to Buy (OTB) check'),
      '#description' => $this->t('Open to Buy (OTB) allows you to check that a card is still valid and active without actually processing a payment against it. Please make sure this feature is enabled for your Global Payments account before enabling it in Drupal.'),
      '#default_value' => $this->configuration['otb_check'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['payer_type'] = $values['payer_type'];
      $this->configuration['otb_check'] = $values['otb_check'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    // Hosted Payment Pages mode assumes that capture will happen after user
    // confirms transaction on client side.
    if (!empty($capture)) {
      return;
    }
    // HPP API.
    $config = $this->initServiceInstance();

    $config->hostedPaymentConfig = new HostedPaymentConfig();
    $config->hostedPaymentConfig->version = HppVersion::VERSION_2;
    $config->hostedPaymentConfig->cardStorageEnabled = "1";
    $config->hostedPaymentConfig->paymentButtonText = $this->configuration['button_label'];

    $service = new HostedService($config);
    $hpp_payment_data = new HostedPaymentData();
    $hpp_payment_data->customerExists = FALSE;

    // BREAKING CHANGE: payment key doesn't contain card type any more because
    // at this stage in payment processing there is no card yet.
    $hpp_payment_data->customerKey = $this->getReference($payment->getOrderId());
    $hpp_payment_data->paymentKey = $this->getReference($payment->getOrderId());

    $hpp_billing_address = $this->prepareBillingData($payment, $hpp_payment_data);

    \Drupal::logger('commerce_globalpayments')->debug(
      'Global Payments HPP Card Storage initialisation request data: <pre>@hpp_payment_data</pre>, <pre>@hpp_billing_address</pre>',
      [
        '@hpp_payment_data' => print_r($hpp_payment_data, TRUE),
        '@hpp_billing_address' => print_r($hpp_billing_address, TRUE),
        'link' => $payment->getOrder()->toLink('View order')->toString(),
      ]
    );

    if ($this->configuration['otb_check']) {
      // Validate the card using Open To Buy (OTB).
      // It will check that the card is still valid and active without
      // actually processing a payment against it.
      $response = $service->verify(0);
    }
    else {
      $response = $service->charge($payment->getAmount()->getNumber());
    }

    $remote_json = $response
      ->withCurrency($payment->getAmount()->getCurrencyCode())
      ->withHostedPaymentData($hpp_payment_data)
      ->withAddress($hpp_billing_address, AddressType::BILLING)
      ->serialize();

    \Drupal::logger('commerce_globalpayments')->debug(
      'Global Payments HPP Card Storage initialisation response: <pre>@remote_json</pre>',
      [
        '@remote_json' => $remote_json,
        'link' => $payment->getOrder()->toLink('View order')->toString(),
      ]
    );

    $remote_json_parsed = \GuzzleHttp\json_decode($remote_json, TRUE);
    if (!empty($remote_json_parsed['ORDER_ID'])) {
      $payment_method->setRemoteId($remote_json_parsed['ORDER_ID']);
      $payment->setRemoteId($remote_json_parsed['ORDER_ID']);
    }

    $payment_method->set('remote_json', $remote_json);
    $payment_method->save();

    // Explicitly save payment to make sure its ID is generated and available
    // in REST normalizer.
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $remote_response = \Drupal::request()->getContent();
    $service = new HostedService($this->initServiceInstance());
    \Drupal::logger('commerce_globalpayments')->debug(
      'Global Payments HPP Card Storage capture response: <pre>@remote_response</pre>',
      [
        '@remote_response' => $remote_response,
        'link' => $payment->getOrder()->toLink('View order')->toString(),
      ]
    );

    $array_remote_response = json_decode($remote_response, true);
    // Check if GP response contains responseCode.
    if (empty($array_remote_response["RESULT"])) {
      \Drupal::logger('commerce_globalpayments')->error(
        'Global Payments HPP error: Incorrect response from GP for order @order_id. Response: <pre>@remote_response</pre>',
        [
          '@remote_response' => $remote_response,
          '@order_id' => $payment->getOrderId(),
          'link' => $payment->getOrder()->toLink('View order')->toString(),
        ]
      );
      throw new DeclineException('Could not process credit card payment.');
    }

    $parsed_response = $service->parseResponse($remote_response);
    $response_values = $parsed_response->responseValues;

    if ($parsed_response->responseCode !== '00') {
      \Drupal::logger('commerce_globalpayments')->error(
        'Global Payments HPP Card Storage error: @message (code @status_code / order @order_id)',
        [
          '@status_code' => $parsed_response->responseCode,
          '@message' => $parsed_response->responseMessage,
          '@order_id' => $payment->getOrderId(),
          'link' => $payment->getOrder()->toLink('View order')->toString(),
        ]
      );
      $payment->setState('authorization_voided');
      $payment->save();
      throw new DeclineException('Could not process credit card payment.');
    }

    $required_fields = [
      'SAVED_PMT_EXPDATE',
      'SAVED_PMT_TYPE',
      'SAVED_PAYER_REF',
      'SAVED_PMT_REF',
      'SAVED_PMT_NAME',
    ];
    $missed_required_fields = [];
    foreach ($required_fields as $required_field) {
      if (empty($response_values[$required_field])) {
        $missed_required_fields[] = $required_field;
      }
    }

    if (!empty($missed_required_fields)) {
      \Drupal::logger('commerce_globalpayments')->error(
        "Global Payments HPP Card Storage response doesn't contain all required data. Missed fields: %field.",
        [
          '%field' => implode(', ', $missed_required_fields),
        ]
      );
      throw new DeclineException('Could not process credit card payment. Please contact site administrator.');
    }

    $card_exp_month = substr($response_values['SAVED_PMT_EXPDATE'], 0, 2);
    $card_exp_year = '20' . substr($response_values['SAVED_PMT_EXPDATE'], -2);

    $payment_method->card_type = $response_values['SAVED_PMT_TYPE'];
    $payment_method->card_number = substr($response_values['SAVED_PMT_DIGITS'], -4);
    $payment_method->card_exp_month = $card_exp_month;
    $payment_method->card_exp_year = $card_exp_year;
    $expires = CreditCard::calculateExpirationTimestamp($card_exp_month, $card_exp_year);
    $payment_method->setExpiresTime($expires);
    $payment_method->payer_reference = $response_values['SAVED_PAYER_REF'];
    $payment_method->card_reference = $response_values['SAVED_PMT_REF'];
    $payment_method->save();

    try {
      // Update customer name in Global Payments (by default customer has all
      // fields empty).
      if (!empty($response_values['SAVED_PMT_NAME'])) {
        // Customer name was returned by Global Payments so we don't need to
        // sanitize it. Break it into 2 pieces and save on customer level.
        $name_parts = explode(' ', trim($response_values['SAVED_PMT_NAME']));
        $count = count($name_parts);
        $first_name = $name_parts[0];
        $last_name = !empty($name_parts[1]) ? $name_parts[1] : '';

        if ($count > 2) {
          $last_name = $name_parts[$count - 1];
          $first_name = implode(' ', array_slice($name_parts, 0, -1));
        }

        $this->initServiceInstance(self::MODE_API);
        $customer = new Customer();
        $customer->key = $response_values['SAVED_PAYER_REF'];
        $customer->firstName = $first_name;
        $customer->lastName = $last_name;
        $customer->saveChanges();
      }
    }
    catch (\Throwable $exception) {
      \Drupal::logger('commerce_globalpayments')->error(
        "Global Payments HPP card stored, but customer name and email were NOT updated in Global Payments storage. Order: @id. Error: %error",
        [
          '@id' => $payment->getOrderId(),
          '%error' => $exception->getMessage(),
        ]
      );
    }

    $payment->setState('completed');
    $payment->save();
  }

}
