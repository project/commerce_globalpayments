<?php

namespace Drupal\commerce_globalpayments\Plugin\Commerce\PaymentGateway;

use GlobalPayments\Api\Entities\Enums\AddressType;
use GlobalPayments\Api\Entities\Enums\HppVersion;
use GlobalPayments\Api\Entities\HostedPaymentData;
use GlobalPayments\Api\HostedPaymentConfig;
use GlobalPayments\Api\Services\HostedService;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_price\Price;

/**
 * Provides Global Payments (Realex) HPP payment gateway for credit cards.
 *
 * @CommercePaymentGateway(
 *   id = "globalpayments_hpp_creditcard",
 *   label = "Global Payments HPP Credit Card",
 *   display_label = "Global Payments HPP Credit Card",
 *   payment_method_types = {"globalpayments_credit_card"},
 * )
 */
class GlobalPaymentsHPPCreditCard extends GlobalPaymentsHPPBase {

  /**
   * {@inheritdoc}
   */
  public function createPaymentMethod(PaymentMethodInterface $payment_method, array $payment_details) {
    // Create new payment method.
    $payment_method->setReusable(FALSE);
    $payment_method->save();

    // Pass payment details further. It won't save anything in the database
    // because payment method type doesn't have fields apart from these:
    // PaymentMethodType\CreditCard::buildFieldDefinitions.
    $payment_method->payment_details = $payment_details;
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    // Hosted Payment Pages mode assumes that capture will happen after user
    // confirms transaction on client side.
    if (!empty($capture)) {
      return;
    }

    // HPP API.
    $config = $this->initServiceInstance();

    $config->hostedPaymentConfig = new HostedPaymentConfig();
    $config->hostedPaymentConfig->version = HppVersion::VERSION_2;
    $config->hostedPaymentConfig->paymentButtonText = $this->configuration['button_label'];

    $service = new HostedService($config);
    $hpp_payment_data = new HostedPaymentData();

    $hpp_billing_address = $this->prepareBillingData($payment, $hpp_payment_data);

    \Drupal::logger('commerce_globalpayments')->debug(
      'Global Payments HPP initialisation request data: <pre>@hpp_payment_data</pre>, <pre>@hpp_billing_address</pre>',
      [
        '@hpp_payment_data' => print_r($hpp_payment_data, TRUE),
        '@hpp_billing_address' => print_r($hpp_billing_address, TRUE),
        'link' => $payment->getOrder()->toLink('View order')->toString(),
      ]
    );

    $remote_json = $service->charge($payment->getAmount()->getNumber())
      ->withCurrency($payment->getAmount()->getCurrencyCode())
      ->withHostedPaymentData($hpp_payment_data)
      ->withAddress($hpp_billing_address, AddressType::BILLING)
      ->serialize();

    \Drupal::logger('commerce_globalpayments')->debug(
      'Global Payments HPP initialisation response: <pre>@remote_json</pre>',
      [
        '@remote_json' => $remote_json,
        'link' => $payment->getOrder()->toLink('View order')->toString(),
      ]
    );

    $remote_json_parsed = \GuzzleHttp\json_decode($remote_json, TRUE);
    if (!empty($remote_json_parsed['ORDER_ID'])) {
      $payment_method->setRemoteId($remote_json_parsed['ORDER_ID']);
      $payment->setRemoteId($remote_json_parsed['ORDER_ID']);
    }

    $payment_method->set('remote_json', $remote_json);
    $payment_method->save();

    // Explicitly save payment to make sure its ID is generated and available
    // in REST normalizer.
    $payment->save();
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $remote_response = \Drupal::request()->getContent();
    $service = new HostedService($this->initServiceInstance());
    \Drupal::logger('commerce_globalpayments')->debug(
      'Global Payments HPP capture response: <pre>@remote_response</pre>',
      [
        '@remote_response' => $remote_response,
        'link' => $payment->getOrder()->toLink('View order')->toString(),
      ]
    );

    $array_remote_response = json_decode($remote_response, true);
    // Check if GP response contains responseCode.
    if (empty($array_remote_response["RESULT"])) {
      \Drupal::logger('commerce_globalpayments')->error(
        'Global Payments HPP error: Incorrect response from GP for order @order_id. Response: <pre>@remote_response</pre>',
        [
          '@remote_response' => $remote_response,
          '@order_id' => $payment->getOrderId(),
          'link' => $payment->getOrder()->toLink('View order')->toString(),
        ]
      );
      throw new DeclineException('Could not process credit card payment.');
    }

    $parsed_response = $service->parseResponse($remote_response);
    $response_values = $parsed_response->responseValues;

    if ($parsed_response->responseCode !== '00') {
      \Drupal::logger('commerce_globalpayments')->error(
        'Global Payments HPP error: @message (code @status_code / order @order_id)',
        [
          '@status_code' => $parsed_response->responseCode,
          '@message' => $parsed_response->responseMessage,
          '@order_id' => $payment->getOrderId(),
          'link' => $payment->getOrder()->toLink('View order')->toString(),
        ]
      );
      $payment->setState('authorization_voided');
      $payment->save();
      throw new DeclineException('Could not process credit card payment.');
    }

    // Log critical error if amounts in Drupal and in Global Payments are
    // different. There is nothing else we can do at this stage because money
    // has been already charged.
    $payment_amount = intval($this->toMinorUnits($payment->getAmount()));
    $response_amount = intval($response_values['AMOUNT']);
    if ($payment_amount !== $response_amount) {
      \Drupal::logger('commerce_globalpayments')->critical(
        'Different amounts in Global Payments HPP and in Drupal. Payment was processed anyway. Order: @order_id. Amounts: @amount1 vs @amount2',
        [
          '@order_id' => $payment->getOrderId(),
          'link' => $payment->getOrder()->toLink('View order')->toString(),
          '@amount1' => $payment->getAmount(),
          '@amount2' => $response_amount / 100,
        ]
      );
    }

    $payment->setState('completed');
    $payment->save();
  }

}
