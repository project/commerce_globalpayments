<?php

namespace Drupal\commerce_globalpayments\Plugin\Commerce\PaymentGateway;

use com\realexpayments\remote\sdk\domain\Card;
use com\realexpayments\remote\sdk\domain\Payer;
use com\realexpayments\remote\sdk\domain\payment\PaymentType;
use com\realexpayments\remote\sdk\domain\PresenceIndicator;
use Drupal\commerce_payment\CreditCard;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides Global Payments credit card storage gateway.
 *
 * IMPORTANT: it does NOT charge the card. Instead, it validates the card and
 * saves it in Realex Vault for later processing. You can use this gateway if
 * you need to collect cards data and then process payments either in Realex
 * Control Panel or using your custom integration.
 * See https://developer.realexpayments.com/#!/integration-api/card-storage/php/html_js.
 *
 * @CommercePaymentGateway(
 *   id = "globalpayments_creditcard_storage",
 *   label = "Global Payments - Store Credit Card in Realex vault",
 *   display_label = "Global Payments - Store Credit Card in Realex vault",
 *   payment_method_types = {"globalpayments_credit_card"},
 * )
 */
class GlobalPaymentsCreditCardStorage extends GlobalPaymentsCreditCard {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'card_types' => [],
      'payer_type' => 'Drupal',
      'otb_check' => 0,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['payer_type'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Payer type in Global Payments'),
      '#description' => $this->t('Created payers will be marked with the specified type in <a href="@link">RealControl</a>. It may be useful for managing payers from different sources.', ['@link' => 'https://realcontrol.realexpayments.com']),
      '#default_value' => $this->configuration['payer_type'],
    ];

    $form['otb_check'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Open to Buy (OTB) check'),
      '#description' => $this->t('Open to Buy (OTB) allows you to check that a card is still valid and active without actually processing a payment against it. Please make sure this feature is enabled for your Global Payments account before enabling it in Drupal.'),
      '#default_value' => $this->configuration['otb_check'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['payer_type'] = $values['payer_type'];
      $this->configuration['otb_check'] = $values['otb_check'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPayment(PaymentInterface $payment, $capture = TRUE) {
    $this->assertPaymentState($payment, ['new']);
    $payment_method = $payment->getPaymentMethod();
    $this->assertPaymentMethod($payment_method);

    $payment_details = $payment_method->payment_details;

    // Save payment in new state for better logging.
    $payment->setState('new');
    $payment->save();

    // Format expiry for Global Payments API.
    $expiry = self::formatExpiry($payment_details['expiration']['month'], $payment_details['expiration']['year']);
    $account = $payment_method->getOwner();

    // Prepare unique payer and card references for Realex.
    $payer_reference = $this->getReference($payment->getOrderId());
    $card_reference = $this->getReference($payment->getOrderId() . '_' . $payment_method->card_type->value);

    // Create a card.
    $card = (new Card())
      ->addReference($card_reference)
      ->addNumber($payment_details['number'])
      ->addType(self::formatCardType($payment_method->card_type->value))
      ->addCardHolderName($payment_details['name'])
      ->addCvn($payment_details['security_code'])
      ->addCvnPresenceIndicator(PresenceIndicator::CVN_PRESENT)
      ->addExpiryDate($expiry);

    // Validate the card using Open To Buy (OTB).
    // It will check that the card is still valid and active without actually
    // processing a payment against it.
    if (!empty($this->configuration['otb_check'])) {
      $response = NULL;

      $request = $this->request()
        ->addType(PaymentType::OTB)
        ->addCard($card);

      // Realex SDK throws exception if OTB is not enabled in this account.
      // Catch it and log in whatchdog.
      try {
        $response = $this->api->send($request);
      }
      catch (\Exception $e) {
        watchdog_exception('commerce_globalpayments', $e);
      }

      if (!empty($response) && !$response->isSuccess()) {
        \Drupal::logger('commerce_globalpayments')->error(
          'Recurring Credit card payment error from Global Payments API: @message (code @status_code / order @order_id)',
          [
            '%id' => $payment->id(),
            '@status_code' => $response->getResult(),
            '@message' => $response->getMessage(),
            '@order_id' => $payment->getOrderId(),
            'link' => $payment->getOrder()->toLink('View order')->toString(),
          ]
        );
        throw new DeclineException('Credit card verification failed.');
      }
    }

    // Create a payer.
    $payer = (new Payer())
      ->addType($this->configuration['payer_type'])
      ->addRef($payer_reference);

    // Pass first / last name from address.
    // NOTE: don't pass email because Realex API doesn't allow email addresses
    // with special characters like +.
    /** @var \Drupal\address\Plugin\Field\FieldType\AddressItem $billing_address */
    if ($billing_address = $payment_method->getBillingProfile()) {
      $billing_address = $payment_method->getBillingProfile()->get('address')->first();
      if (!empty($billing_address)) {
        $payer->setFirstName($billing_address->getGivenName());
        $payer->setSurname($billing_address->getFamilyName());
      }
    }

    $request = $this->request()
      ->addType(PaymentType::PAYER_NEW)
      ->addPayer($payer);

    $response = $this->api->send($request);
    if (!$response->isSuccess()) {
      throw new DeclineException('Could not process payment.');
    }

    // Connect the payer and the card.
    $card->addPayerReference($payer_reference);
    // Save the card in Realex.
    $request = $this->request()
      ->addType(PaymentType::CARD_NEW)
      ->addCard($card);

    $response = $this->api->send($request);
    if (!$response->isSuccess()) {
      throw new DeclineException('Could not save the credit card.');
    }

    $expires = CreditCard::calculateExpirationTimestamp($payment_details['expiration']['month'], $payment_details['expiration']['year']);
    $payment_method->setExpiresTime($expires);
    $payment_method->payer_reference = $payer_reference;
    $payment_method->card_reference = $card_reference;

    // Store payer reference on user level.
    // This feature currently is not used but may be used in the future.
    $this->setRemoteCustomerId($account, $payer_reference);

    // Global Payments payments id is stored on payment method level.
    $payment_method->setRemoteId($response->getPaymentsReference());
    $payment_method->save();

    $payment->setState('completed');
    // Use payer reference as a remote payment id.
    $payment->setRemoteId($payer_reference);
    $payment->save();
  }

}
